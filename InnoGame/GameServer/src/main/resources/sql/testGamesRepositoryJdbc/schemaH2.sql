drop table if exists game;
drop table if exists player;

create table player
(
    id_player          long auto_increment primary key,
    ip_last_connect    varchar(45) not null,
    name_player        varchar(50) not null,
    max_points         integer default 0,
    numbers_of_wins    integer default 0,
    numbers_of_defeats integer default 0
);

create table game
(
    id_game                   long auto_increment primary key,
    date_time                 timestamp,
    first_player              varchar(50) not null,
    second_player             varchar(50) not null,
    count_shots_first_player  integer default 0,
    count_shots_second_player integer default 0,
    seconds_total_time_game   bigint  default 0,
    foreign key (first_player) references player(name_player),
    foreign key (second_player) references player(name_player)
);
