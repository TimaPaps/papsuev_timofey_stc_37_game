package ru.inno.server.socket;

import static ru.inno.server.constants.Constants.*;

/**
 * 06.05.2021 0:32
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class CommandsParser {
    /**
     * Метод, сообщение о получении никнейма игрока.
     *
     * @param messageFromClient - сообщение от клиента.
     * @return - true, если сообщение содержит константу префикс для никнейма.
     */
    protected static boolean isMessageForNickname(String messageFromClient) {
        return messageFromClient.startsWith(PREFIX_FOR_NICKNAME);
    }

    /**
     * Метод, сообщение для выхода из игры.
     *
     * @param messageFromClient - сообщение от клиента.
     * @return - true, если сообщение равно константе для выхода из игры.
     */
    protected static boolean isMessageForExit(String messageFromClient) {
        return messageFromClient.equals(EXIT);
    }

    /**
     * Метод, сообщает о перемещении игрока влево или вправо.
     *
     * @param messageFromClient - сообщение от клиента.
     * @return - строка, сообщение.
     */
    protected static boolean isMessageForMove(String messageFromClient) {
        return messageFromClient.equals(STEP_LEFT) || messageFromClient.equals(STEP_RIGHT);
    }

    /**
     * Метод, сообщает о произведении выстрела игроком.
     *
     * @param messageFromClient - сообщение от клиента.
     * @return - строка, сообщение.
     */
    protected static boolean isMessageForShot(String messageFromClient) {
        return messageFromClient.equals(SHOT);
    }

    /**
     * Метод, сообщает о попадании пули в цель.
     *
     * @param messageFromClient - сообщение от клиента.
     * @return - строка, сообщение.
     */
    protected static boolean isMessageForDamage(String messageFromClient) {
        return messageFromClient.equals(MESSAGE_DAMAGE);
    }
}
