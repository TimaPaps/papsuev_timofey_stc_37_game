package ru.inno.server.repository;

import ru.inno.server.algorithms.GeneratorId;
import ru.inno.model.models.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 31.03.2021 10:19
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class PlayersRepositoryFileImpl implements PlayersRepository {
    /**
     * Строка, разделитель.
     */
    private static final String SEPARATOR = "#";
    /**
     * Имя файла, хранит данные объектов типа Player.
     */
    private final String dbFileName;
    /**
     * Имя файла, хранит номер id объекта типа Player, под которым будут сохранены данные нового объекта.
     */
    private final String sequenceFileName;
    /**
     * Строка, текущий игрок полученный из базы данных.
     */
    private String currentPlayerFromDb;

    public PlayersRepositoryFileImpl(String dbFileName, String sequenceFileName) {
        this.dbFileName = dbFileName;
        this.sequenceFileName = sequenceFileName;
    }

    @Override
    public void save(Player player) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true))) {
            player.setIdPlayer(GeneratorId.generateId(sequenceFileName));

            writer.write(player.getIdPlayer() + "#"
                    + player.getIpLastConnect() + "#"
                    + player.getNamePlayer() + "#"
                    + player.getMaxPoints() + "#"
                    + player.getNumbersOfWins() + "#"
                    + player.getNumbersOfDefeats() + "\n");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Player findByNickname(String nickname) {
        try (BufferedReader reader = new BufferedReader(new FileReader(dbFileName))) {

            while (true) {
                currentPlayerFromDb = reader.readLine();

                if (currentPlayerFromDb == null) {
                    return null;
                }

                if (!currentPlayerFromDb.isEmpty()) {
                    String[] tokens = currentPlayerFromDb.split(SEPARATOR);

                    if (tokens[2].equals(nickname)) {
                        return Player.builder()
                                .ipLastConnect(tokens[1])
                                .namePlayer(tokens[2])
                                .maxPoints(Integer.parseInt(tokens[3]))
                                .numbersOfWins(Integer.parseInt(tokens[4]))
                                .numbersOfDefeats(Integer.parseInt(tokens[5]))
                                .build();
                    }
                } else {
                    break;
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }

    @Override
    public void update(Player player) {
        List<String> listPlayers = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(dbFileName))) {
            while (true) {
                currentPlayerFromDb = reader.readLine();

                if (currentPlayerFromDb == null) {
                    break;
                }

                if (!currentPlayerFromDb.isEmpty()) {
                    listPlayers.add(currentPlayerFromDb);
                } else {
                    break;
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName))) {
            for (int i = 0; i < listPlayers.size(); i++) {
                String temp = listPlayers.get(i);
                String[] tokens = temp.split(SEPARATOR);
                if (tokens[2].equals(player.getNamePlayer())) {
                    tokens[3] = String.valueOf(player.getMaxPoints());
                    tokens[4] = String.valueOf(player.getNumbersOfWins());
                    tokens[5] = String.valueOf(player.getNumbersOfDefeats());
                    String currentPlayer = tokens[0] + "#"
                            + tokens[1] + "#"
                            + tokens[2] + "#"
                            + tokens[3] + "#"
                            + tokens[4] + "#"
                            + tokens[5];
                    listPlayers.set(i, currentPlayer);
                    break;
                }
            }

            for (String players : listPlayers) {
                writer.write(players + "\n");
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
