package ru.inno.server.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 16.04.2021 11:54
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface RowMapper<T> {
    T mapRow(ResultSet row) throws SQLException;
}
