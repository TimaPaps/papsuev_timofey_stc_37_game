package ru.inno.server.configuration;

import com.zaxxer.hikari.HikariConfig;

import static ru.inno.server.configuration.Configuration.*;

/**
 * 20.05.2021 15:12
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class ConfigurationHikari extends HikariConfig {
    /**
     * Метод, конфигурация для объекта типа HikariConfig.
     *
     * @return - объект типа HikariConfig.
     */
    public static HikariConfig getHikariConfig() {
        HikariConfig configuration = new HikariConfig();
        configuration.setDriverClassName(getDriverJdbc());
        configuration.setJdbcUrl(getUrlJdbc());
        configuration.setUsername(getUserJdbc());
        configuration.setPassword(getPasswordJdbc());
        configuration.setMaximumPoolSize(10);
        return configuration;
    }
}
