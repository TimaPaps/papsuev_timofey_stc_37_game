package ru.inno.server.repository;

import ru.inno.server.algorithms.GeneratorId;
import ru.inno.model.models.Shot;

import java.io.*;

/**
 * 30.03.2021 23:30
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class ShotsRepositoryFileImpl implements ShotsRepository {
    /**
     * Имя файла, хранит данные объектов типа Shot.
     */
    private final String dbFileName;
    /**
     * Имя файла, хранит номер id объекта типа Shot, под которым будут сохранены данные нового объекта.
     */
    private final String sequenceFileName;

    public ShotsRepositoryFileImpl(String dbFileName, String sequenceFileName) {
        this.dbFileName = dbFileName;
        this.sequenceFileName = sequenceFileName;
    }

    @Override
    public void save(Shot shot) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true))) {
            shot.setIdShot(GeneratorId.generateId(sequenceFileName));

            writer.write(shot.getIdShot() + "#"
                    + shot.getShotTime().toString() + "#"
                    + shot.getCurrentIdGame().getIdGame() + "#"
                    + shot.getPlayerShot().getNamePlayer() + "#"
                    + shot.getPlayerTarget().getNamePlayer() + "\n");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
