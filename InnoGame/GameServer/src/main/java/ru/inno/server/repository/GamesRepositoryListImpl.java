package ru.inno.server.repository;

import ru.inno.model.models.Game;

import java.util.ArrayList;
import java.util.List;

/**
 * 26.03.2021 18:22
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class GamesRepositoryListImpl implements GamesRepository {
    /**
     * Список хранит объекты типа Game.
     */
    private final List<Game> games;

    public GamesRepositoryListImpl() {
        games = new ArrayList<>();
    }

    @Override
    public void save(Game game) {
        game.setIdGame((long) games.size());
        games.add(game);
    }

    @Override
    public Game findById(Long idGame) {
        return games.get(idGame.intValue());
    }

    @Override
    public void update(Game game) {
        games.set(game.getIdGame().intValue(), game);
    }
}
