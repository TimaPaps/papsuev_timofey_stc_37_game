package ru.inno.server.repository;

import ru.inno.model.models.Game;

/**
 * 26.03.2021 17:43
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface GamesRepository {
    /**
     * Метод сохраняет объект типа Game в репозитории.
     *
     * @param game - объект типа Game.
     */
    void save(Game game);

    /**
     * Метод осуществляет поиск объекта типа Game по его id в репозитории.
     *
     * @param idGame - id объекта (игры) типа Game.
     * @return - объект типа Game.
     */
    Game findById(Long idGame);

    /**
     * Метод обновляет данные объекта типа Game в репозитории.
     *
     * @param game - объект типа Game.
     */
    void update(Game game);
}
