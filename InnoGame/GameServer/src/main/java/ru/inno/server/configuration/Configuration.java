package ru.inno.server.configuration;

/**
 * 30.04.2021 13:24
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Configuration {
    //TODO: вынести константы в файл.
    /**
     * Строка, драйвер JDBC.
     */
    private static final String DRIVER_JDBC = "org.postgresql.Driver";
    /**
     * Строка, адрес базы данных.
     */
    private static final String URL_JDBC = "jdbc:postgresql://localhost:5432/stc_37_game";
    /**
     * Строка, пользователь базы данных.
     */
    private static final String USER_JDBC = "tima";
    /**
     * Строка, пароль пользователя базы данных.
     */
    private static final String PASSWORD_JDBC = "";
    /**
     * Число, порт для подключения к серверу.
     */
    private static final int SERVER_PORT = 7777;
    /**
     * Строка, путь к репозиторию с данными на игроков.
     */
    private static final String DB_FILE_NAME_PLAYER = "GameServer/src/main/resources/txt/players_db.txt";
    /**
     * Строка, путь к файлу хранящему число - id для нового игрока.
     */
    private static final String SEQUENCE_FILE_NAME_PLAYER = "GameServer/src/main/resources/txt/players_sequence.txt";
    /**
     * Строка, путь к репозиторию с данными по выстрелам.
     */
    private static final String DB_FILE_NAME_SHOT = "GameServer/src/main/resources/txt/shots_db.txt";
    /**
     * Строка, путь к файлу хранящему число - id для нового выстрела.
     */
    private static final String SEQUENCE_FILE_NAME_SHOT = "GameServer/src/main/resources/txt/shots_sequence.txt";

    public static String getDriverJdbc() {
        return DRIVER_JDBC;
    }

    public static String getUrlJdbc() {
        return URL_JDBC;
    }

    public static String getUserJdbc() {
        return USER_JDBC;
    }

    public static String getPasswordJdbc() {
        return PASSWORD_JDBC;
    }

    public static int getServerPort() {
        return SERVER_PORT;
    }

    public static String getDbFileNamePlayer() {
        return DB_FILE_NAME_PLAYER;
    }

    public static String getSequenceFileNamePlayer() {
        return SEQUENCE_FILE_NAME_PLAYER;
    }

    public static String getDbFileNameShot() {
        return DB_FILE_NAME_SHOT;
    }

    public static String getSequenceFileNameShot() {
        return SEQUENCE_FILE_NAME_SHOT;
    }
}
