package ru.inno.model.models;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 23.03.2021 16:45
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
@Builder
public class Shot {
    /**
     * Идентификатор выстрела.
     */
    private Long idShot;
    /**
     * Время выстрела.
     */
    private LocalDateTime shotTime;
//    /**
//     * Выстрел в цель
//     */
//    private Boolean shotTheTarget;
    /**
     * Ссылка на объект типа Game - имя игры.
     */
    private Game currentIdGame;
    /**
     * Ссылка на объект типа Player - игрок стрелок.
     */
    private Player playerShot;
    /**
     * Ссылка на объект типа Player - игрок цель.
     */
    private Player playerTarget;
}
