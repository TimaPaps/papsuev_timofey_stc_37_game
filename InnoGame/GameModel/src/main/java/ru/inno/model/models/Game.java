package ru.inno.model.models;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 23.03.2021 16:45
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
@Builder
public class Game {
    /**
     * Идентификатор игры.
     */
    private Long idGame;
    /**
     * Дата и время игры.
     */
    private LocalDateTime dateTime;
    /**
     * Ссылка на объект типа Player - первый игрок.
     */
    private Player firstPlayer;
    /**
     * Ссылка на объект типа Player - второй игрок.
     */
    private Player secondPlayer;
    /**
     * Количество выстрелов попавших в цель первого игрока.
     */
    private Integer countShotsFirstPlayer;
    /**
     * Количество выстрелов попавших в цель второго игрока.
     */
    private Integer countShotsSecondPlayer;
    /**
     * Длительность игры в секундах.
     */
    private Long secondsTotalTimeGame;
}
