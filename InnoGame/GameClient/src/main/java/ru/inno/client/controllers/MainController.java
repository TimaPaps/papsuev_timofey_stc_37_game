package ru.inno.client.controllers;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import ru.inno.client.configuration.Configuration;
import ru.inno.client.exceptions.ExceptionForClient;
import ru.inno.client.socket.SocketClient;
import ru.inno.client.utils.GameUtils;

import java.net.URL;
import java.util.ResourceBundle;

import static ru.inno.client.constants.Constants.*;

/**
 * 03.05.2021 1:07
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class MainController implements Initializable {
    /**
     * Ссылка на объект GameUtils.
     */
    private GameUtils gameUtils;
    /**
     * Канал подключения.
     */
    private SocketClient socketClient;
    /**
     * Панель приложения.
     */
    @FXML
    private AnchorPane fxAnchorPane;
    /**
     * Панель правая (кнопки, поля...).
     */
    @FXML
    private Pane fxRightPane;
    /**
     * Игрок - стрелок.
     */
    @FXML
    private ImageView fxPlayer;
    /**
     * Игрок - цель.
     */
    @FXML
    private ImageView fxEnemy;
    /**
     * Визуальная полоса - состояние игрока-стрелка.
     */
    @FXML
    private Pane fxHealthPlayer;
    /**
     * Визуальная полоса - состояние игрока-цели.
     */
    @FXML
    private Pane fxHealthEnemy;
    /**
     * Картинка для проигравшего в игре.
     */
    @FXML
    private ImageView fxFail;
    /**
     * Надпись - состояние подключения второго игрока.
     */
    @FXML
    private Label fxLabelInfoGame;
    /**
     * Кнопка - соединение с сервером.
     */
    @FXML
    private Button fxButtonConnect;
    /**
     * Текстовое поле - ввод имени игрока.
     */
    @FXML
    private TextField fxTextPlayerNickName;
    /**
     * Кнопка - старт игры (отправление имени игрока на сервер).
     */
    @FXML
    private Button fxButtonStartGame;
    /**
     * Кнопка - завершение игры.
     */
    @FXML
    private Button fxButtonStopGame;
    /**
     * Поле - вывод сообщений.
     */
    @FXML
    private TextArea fxMessages;

    public AnchorPane getFxAnchorPane() {
        return fxAnchorPane;
    }

    public Pane getFxRightPane() {
        return fxRightPane;
    }

    public ImageView getFxPlayer() {
        return fxPlayer;
    }

    public ImageView getFxEnemy() {
        return fxEnemy;
    }

    public Pane getFxHealthPlayer() {
        return fxHealthPlayer;
    }

    public Pane getFxHealthEnemy() {
        return fxHealthEnemy;
    }

    public ImageView getFxFail() {
        return fxFail;
    }

    public GameUtils getGameUtils() {
        return gameUtils;
    }

    public Label getFxLabelInfoGame() {
        return fxLabelInfoGame;
    }

    public TextField getFxTextPlayerNickName() {
        return fxTextPlayerNickName;
    }

    public Button getFxButtonStartGame() {
        return fxButtonStartGame;
    }

    public Button getFxButtonStopGame() {
        return fxButtonStopGame;
    }

    public EventHandler<KeyEvent> getKeyEventEventHandler() {
        return keyEventEventHandler;
    }

    public TextArea getFxMessages() {
        return fxMessages;
    }

    /**
     * Класс, обработчик событий (нажатие клавиш клавиатуры).
     */
    private final EventHandler<KeyEvent> keyEventEventHandler = keyEvent -> {
        // проверка, если нет подключения, то игрок не может передвигаться и стрелять
        if (socketClient != null && !fxButtonStopGame.isDisable()) {
            if (keyEvent.getCode() == KeyCode.RIGHT) {
                // ограничение двидения вправо
                if (fxPlayer.getX() + fxPlayer.getLayoutX() < fxRightPane.getLayoutX() - fxPlayer.getFitWidth() - 15) {
                    // вращение картинки в сторону перемещения
                    fxPlayer.setRotate(90);
                    // передвигаем игрока вправо
                    gameUtils.goImgRight(fxPlayer);
                }
                // сообщаем второму игроку о передвижении первого вправо
                socketClient.sendMessage(STEP_RIGHT);
            } else if (keyEvent.getCode() == KeyCode.LEFT) {
                // ограничение двидения влево
                if (fxPlayer.getX() + fxPlayer.getLayoutX() > fxAnchorPane.getLayoutX() + 10) {
                    // вращение картинки в сторону перемещения
                    fxPlayer.setRotate(-90);
                    // передвигаем игрока влево
                    gameUtils.goImgLeft(fxPlayer);
                }
                // сообщаем второму игроку о передвижении первого влево
                socketClient.sendMessage(STEP_LEFT);
            } else if (keyEvent.getCode() == KeyCode.SPACE) {
                // вращение картинки в сторону противника
                fxPlayer.setRotate(0);
                // создание пули/выстрела
                gameUtils.createImgBulletFor(fxPlayer, false);
                // сообщаем второму игроку о выстреле первого
                socketClient.sendMessage(SHOT);
            }
        }
    };

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // подключаем класс отвечающий за движения
        gameUtils = new GameUtils();
        // устанавливаем классу этот контроллер
        gameUtils.setMainController(this);

        // создаем поток при нажатии на кнопку
        fxButtonConnect.setOnAction(actionEvent -> {
            try {
                // создаем порт для подключения
                socketClient = new SocketClient(this, Configuration.getHOST(), Configuration.getServerPort()); // this - передаем в конструктор socketClient-а MainController (передаем самого себя, чтобы классы знали друг о друге 8) )
                // создаем поток - игрок
                new Thread(socketClient).start();
                // делаем не активной кнопку подключения к серверу
                fxButtonConnect.setDisable(true);
                // устанавливаем текст
                fxLabelInfoGame.setText("waiting for the second player to connect!");
                // показываем надпись ожидания второго игрока
                fxLabelInfoGame.setVisible(true);
                // добавляем панель приложения в GameUtils при подключении (тогда, когда созданы все компоненты)
                gameUtils.setAnchorPane(fxAnchorPane);
                // устанавливаем классу клиента
                gameUtils.setSocketClient(socketClient);
            } catch (ExceptionForClient e) {
                fxLabelInfoGame.setText("server is not running!" + "\n" + "try again later");
                fxLabelInfoGame.setVisible(true);
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        });

        // отправляем сообщение на сервер
        fxButtonStartGame.setOnAction(actionEvent -> {
            socketClient.sendMessage(PREFIX_FOR_NICKNAME + fxTextPlayerNickName.getText());
            fxLabelInfoGame.setText("waiting for the name of the second player!");
            // делаем не активными поле ввода и кнопку старт игры после отправки сообщения с именем на сервер
            fxButtonStartGame.setDisable(true);
            fxTextPlayerNickName.setDisable(true);
        });

        // завершение игры
        fxButtonStopGame.setOnAction(actionEvent -> socketClient.sendMessage(EXIT));
    }
}
