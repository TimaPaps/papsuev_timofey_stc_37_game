package ru.inno.server.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.server.configuration.ConfigurationHikari;
import ru.inno.server.repository.*;
import ru.inno.server.services.GameService;
import ru.inno.server.services.GameServiceImpl;
import ru.inno.server.socket.SocketServer;
import ru.inno.server.configuration.Configuration;

import javax.sql.DataSource;

/**
 * 27.04.2021 12:06
 * 39.Socket IO
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class MainServer {
    public static void main(String[] args) {

        DataSource dataSource = new HikariDataSource(ConfigurationHikari.getHikariConfig());

        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);
        SocketServer socketServer = new SocketServer(gameService);
        socketServer.start(Configuration.getServerPort());
    }
}
