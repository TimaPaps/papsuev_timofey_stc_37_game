package ru.inno.server.repository;

import ru.inno.model.models.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * 26.03.2021 18:04
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class PlayersRepositoryMapImpl implements PlayersRepository {
    /**
     * Карта хранит объекты типа Player.
     */
    private final Map<String, Player> players;

    public PlayersRepositoryMapImpl() {
        players = new HashMap<>();
    }

    @Override
    public void save(Player player) {
        player.setIdPlayer((long)players.size());
        players.put(player.getNamePlayer(), player);
    }

    @Override
    public Player findByNickname(String nickname) {
        return players.get(nickname);
    }

    @Override
    public void update(Player player) {
        if (players.containsKey(player.getNamePlayer())) {
            players.put(player.getNamePlayer(), player);
        } else {
            System.err.println("Нельзя обновить данные у несуществующего игрока");
        }
    }
}
