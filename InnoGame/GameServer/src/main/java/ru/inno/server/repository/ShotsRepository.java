package ru.inno.server.repository;

import ru.inno.model.models.Shot;

/**
 * 27.03.2021 12:09
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface ShotsRepository {
    /**
     * Метод сохраняет объект типа Shot в репозитории.
     *
     * @param shot - объект типа Shot.
     */
    void save(Shot shot);
}
