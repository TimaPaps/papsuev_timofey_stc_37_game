package ru.inno.client.exceptions;

/**
 * 12.05.2021 11:37
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class ExceptionForClient extends RuntimeException {
    public ExceptionForClient(String s) {
        super(s);
    }

    public ExceptionForClient(String s, Throwable throwable) {
        super(s, throwable);
    }
}
