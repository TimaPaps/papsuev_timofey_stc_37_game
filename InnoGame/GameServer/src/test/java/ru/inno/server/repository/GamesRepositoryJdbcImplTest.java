package ru.inno.server.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.inno.model.models.Game;
import ru.inno.model.models.Player;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 20.05.2021 17:40
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName("Test for GamesRepositoryJdbc")
@ExtendWith(MockitoExtension.class)
class GamesRepositoryJdbcImplTest {

    @Mock
    private Game game;

    @Mock
    private Player firstPlayer;

    @Mock
    private Player secondPlayer;

    private GamesRepositoryJdbcImpl gamesRepositoryJdbc;

    @Nested
    @DisplayName("save() is working if:")
    class ForSave {
        @BeforeEach
        public void setUp() {
            createH2Database();
            generatorTestObject();
        }

        @Test
        void save_game_to_the_database() {
            gamesRepositoryJdbc.save(game);
        }
    }

    @Nested
    @DisplayName("findById() is working if:")
    class ForFindById {
        @BeforeEach
        public void setUp() {
            createH2Database();
        }

        @ParameterizedTest
        @ValueSource(longs = {1, 2})
        void find_the_game_by_id_in_the_database(long id) {
            assertNotNull(gamesRepositoryJdbc.findById(id));
        }
    }

    @Nested
    @DisplayName("update() is working if:")
    class ForUpdate {
        @BeforeEach
        public void setUp() {
            createH2Database();
            generatorTestObjectForUpdate();
        }

        @Test
        void update_game_to_the_database() {
            gamesRepositoryJdbc.update(game);
        }
    }

    private void createH2Database() {
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("sql/testGamesRepositoryJdbc/schemaH2.sql")
                .addScript("sql/testGamesRepositoryJdbc/dataH2.sql")
                .build();
        gamesRepositoryJdbc = new GamesRepositoryJdbcImpl(dataSource);
    }

    private void generatorTestObject() {
        firstPlayer = new GeneratorTestObjects().playerWithId(77L, "127.0.0.1", "Vasya");
        secondPlayer = new GeneratorTestObjects().playerWithId(88L, "127.0.0.2", "Vova");
        game = new GeneratorTestObjects().gameWithOutId(firstPlayer, secondPlayer);
    }

    private void generatorTestObjectForUpdate() {
        firstPlayer = new GeneratorTestObjects().playerWithIdForUpdate(1L, "127.0.0.11", "Tima", 17, 14, 9);
        secondPlayer = new GeneratorTestObjects().playerWithIdForUpdate(2L, "127.0.0.22", "Dima", 14, 9, 14);
        game = new GeneratorTestObjects().gameWithIdForUpdate(firstPlayer, secondPlayer, 99, 42, 254);
    }
}
