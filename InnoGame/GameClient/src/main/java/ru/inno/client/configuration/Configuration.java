package ru.inno.client.configuration;

/**
 * 03.05.2021 16:48
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Configuration {
    //TODO: вынести константы в файл.
    /**
     *  Хост.
     */
    private static final String HOST = "localhost";
    /**
     * Число, порт для подключения к серверу.
     */
    private static final int SERVER_PORT = 7777;

    public static String getHOST() {
        return HOST;
    }

    public static int getServerPort() {
        return SERVER_PORT;
    }
}
