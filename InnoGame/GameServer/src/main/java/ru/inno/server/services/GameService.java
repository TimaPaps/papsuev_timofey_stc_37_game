package ru.inno.server.services;

import ru.inno.server.dto.StatisticDto;

/**
 * 26.03.2021 14:26
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface GameService {
    /**
     * Метод вызывается для обеспечения начала игры. Если игрок с таким никнеймом уже есть,
     * то мы работаем с ним. Если нет - то создаем нового.
     *
     * @param firstIp              - IP-адрес, с которого зашел первый игрок.
     * @param secondIp             - IP-адрес, с которого зашел второй игрок.
     * @param firstPlayerNickname  - имя первого игрока.
     * @param secondPlayerNickname - имя второго игрока.
     * @return - идентификатор игры.
     */
    Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname);

    /**
     * Метод фиксирует попавший в цель выстрел каждого игрока.
     * @param idGame - идентификатор игры.
     * @param playerShotNickname - игрок стрелок.
     * @param playerTargetNickname - игрок цель.
     */
    void shot(Long idGame, String playerShotNickname, String playerTargetNickname);

    /**
     * Метод обновляет данные объекта типа Game в репозитории и создает объект типа StatisticDto
     * для отображения статистики по игре.
     * @param idGame - id объекта (игры) типа Game.
     * @return - объект типа StatisticDto.
     */
    StatisticDto finishGame(Long idGame);
}
