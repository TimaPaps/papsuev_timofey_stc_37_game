package ru.inno.server.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.inno.model.models.Game;
import ru.inno.model.models.Player;
import ru.inno.model.models.Shot;

import javax.sql.DataSource;

/**
 * 21.05.2021 13:56
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName("Test for ShotsRepositoryJdbc")
@ExtendWith(MockitoExtension.class)
class ShotsRepositoryJdbcImplTest {

    @Mock
    private Shot shot;

    @Mock
    private Game game;

    @Mock
    private Player firstPlayer;

    @Mock
    private Player secondPlayer;

    ShotsRepositoryJdbcImpl shotsRepositoryJdbc;

    @BeforeEach
    public void setUp() {
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("sql/testShotsRepositoryJdbc/schemaH2.sql")
                .build();

        shotsRepositoryJdbc = new ShotsRepositoryJdbcImpl(dataSource);
        generatorTestObjects();
    }

    @Test
    void save_shot_to_the_database() {
        shotsRepositoryJdbc.save(shot);
    }

    private void generatorTestObjects() {
        firstPlayer = new GeneratorTestObjects().playerWithId(1L,"127.0.0.1", "Tima");
        secondPlayer = new GeneratorTestObjects().playerWithId(2L,"127.0.0.2", "Miha");
        game = new GeneratorTestObjects().gameWithId(firstPlayer, secondPlayer);
        shot = new GeneratorTestObjects().shot(game, firstPlayer, secondPlayer);
    }
}