package ru.inno.server.socket;

import ru.inno.server.dto.StatisticDto;
import ru.inno.server.services.GameService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static ru.inno.server.socket.CommandsParser.*;
import static ru.inno.server.constants.Constants.*;

/**
 * 28.04.2021 11:03
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class SocketServer {
    /**
     * Поток для первого игрока.
     */
    private ClientThread firstPlayer;
    /**
     * Поток для второго игрока.
     */
    private ClientThread secondPlayer;
    /**
     * Канал подключения.
     */
    private ServerSocket serverSocket;
    /**
     * Ссылка на объект GameService.
     */
    private final GameService gameService;
    /**
     * Флаг, начало игры, состояние по умолчанию - false.
     */
    private boolean isGameStarted = false;
    /**
     * Флаг, игра в процессе, состояние по умолчанию - true.
     */
    private boolean isGameInProcess = true;
    /**
     * Число, идентификатор игры.
     */
    private long idGame;
    /**
     * Мьютекс.
     */
    private final Lock lock = new ReentrantLock();

    public SocketServer(GameService gameService) {
        this.gameService = gameService;
    }

    /**
     * Метод, запускает сервер на определенном порту.
     *
     * @param port - число, порт на котором будет запущен сервер.
     */
    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("СЕРВЕР ЗАПУЩЕН...");

            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ПЕРВОГО ИГРОКА...");
            firstPlayer = connect();

            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ВТОРОГО ИГРОКА...");
            secondPlayer = connect();

            if (firstPlayer != null && secondPlayer != null) {
                firstPlayer.sendMessage(PLAYERS_CONNECTED);
                secondPlayer.sendMessage(PLAYERS_CONNECTED);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Метод, подключает клиента к серверу.
     *
     * @return - поток, объект-соединение.
     */
    private ClientThread connect() {
        Socket client;
        try {
            client = serverSocket.accept();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        ClientThread clientThread = new ClientThread(client);
        clientThread.start();
        System.out.println("КЛИЕНТ ПОДКЛЮЧЕН...");
        clientThread.sendMessage("Вы подключены к серверу!!!" + "\n" + "Ожидаем подключение к серверу второго игрока!!!");
        return clientThread;
    }

    /**
     * Класс, создает поток для клиента.
     */
    private class ClientThread extends Thread {
        /**
         * Стрим для отправки сообщений на сервер.
         */
        private final PrintWriter toClient;
        /**
         * Стрим для получения сообщений с сервера.
         */
        private final BufferedReader fromClient;
        /**
         * Строка, никнейм клиента.
         */
        private String playerNickname;
        /**
         * Строка, ip игрока подключившегося к игре.
         */
        private final String ipPlayer;

        public String getIpPlayer() {
            return ipPlayer;
        }

        public ClientThread(Socket client) {
            try {
                this.toClient = new PrintWriter(client.getOutputStream(), true);
                this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
                this.ipPlayer = client.getInetAddress().getHostAddress();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        /**
         * Метод, получает сообщения от клиента, запускает и останавливает игру.
         */
        @Override
        public void run() {
            while (isGameInProcess) {
                String messageFromClient;
                try {
                    messageFromClient = fromClient.readLine();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }

                lock.lock();
                if (messageFromClient != null) {
                    if (isMessageForNickname(messageFromClient)) {
                        resolveNickname(messageFromClient);
                    } else if (isMessageForExit(messageFromClient) && isGameInProcess) {
                        lock.lock();
                        StatisticDto statisticDto = gameService.finishGame(idGame);
                        firstPlayer.sendMessage(statisticDto.toString());
                        secondPlayer.sendMessage(statisticDto.toString());
                        firstPlayer.sendMessage(EXIT);
                        secondPlayer.sendMessage(EXIT);
                        System.out.println("ИГРА ЗАВЕРШЕНА!");
                        System.out.println(statisticDto);
                        isGameInProcess = false;
                        try {
                            toClient.close();
                            fromClient.close();
                            serverSocket.close();
                        } catch (IOException e) {
                            throw new IllegalStateException(e);
                        }
                        lock.unlock();
                    } else if (isMessageForMove(messageFromClient)) {
                        resolveMove(messageFromClient);
                    } else if (isMessageForShot(messageFromClient)) {
                        resolveShot(messageFromClient);
                    } else if (isMessageForDamage(messageFromClient)) {
                        resolveDamage();
                    }
                }
                lock.unlock();
                lock.lock();
                if (isReadyForStartGame()) {
                    idGame = gameService.startGame(
                            firstPlayer.getIpPlayer(),
                            secondPlayer.getIpPlayer(),
                            firstPlayer.playerNickname,
                            secondPlayer.playerNickname);
                    firstPlayer.sendMessage(GAME_IS_STARTED);
                    secondPlayer.sendMessage(GAME_IS_STARTED);
                    isGameStarted = true;
                }
                lock.unlock();
            }
        }

        /**
         * Метод, условие для старта игры.
         *
         * @return - true, если первый и второй игроки задали свои имена, и игра еще не началась.
         */
        private boolean isReadyForStartGame() {
            return firstPlayer.playerNickname != null && secondPlayer.playerNickname != null && !isGameStarted;
        }

        /**
         * Метод, определяет для какого игрока сохранить никнейм.
         *
         * @param messageFromClient - сообщение от клиента.
         */
        private void resolveNickname(String messageFromClient) {
            if (isFirst()) {
                fixNickname(messageFromClient, firstPlayer, "ИМЯ ПЕРВОГО ИГРОКА: ", secondPlayer);
            } else {
                fixNickname(messageFromClient, secondPlayer, "ИМЯ ВТОРОГО ИГРОКА: ", firstPlayer);
            }
        }

        /**
         * Метод, отправляет сообщение клиенту.
         *
         * @param messageFromClient - сообщение от клиента.
         */
        private void resolveMove(String messageFromClient) {
            sendMessageForFirstPlayerOrSecondPlayer(messageFromClient);
        }

        /**
         * Метод, отправляет сообщение клиенту.
         *
         * @param messageFromClient - сообщение от клиента.
         */
        private void resolveShot(String messageFromClient) {
            sendMessageForFirstPlayerOrSecondPlayer(messageFromClient);
        }

        /**
         * Метод, фиксирует выстрел попавший в цель.
         */
        private void resolveDamage() {
            if (isFirst()) {
                gameService.shot(idGame, firstPlayer.playerNickname, secondPlayer.playerNickname);
            } else {
                gameService.shot(idGame, secondPlayer.playerNickname, firstPlayer.playerNickname);
            }
        }

        /**
         * Метод, определяет какому клиенту отправить сообщение.
         *
         * @param messageFromClient - сообщение от клиента.
         */
        private void sendMessageForFirstPlayerOrSecondPlayer(String messageFromClient) {
            if (isFirst()) {
                secondPlayer.sendMessage(messageFromClient);
            } else {
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        /**
         * Метод, сохраняет никнейм для текущего игрока и отправляет сообщение второму игроку.
         *
         * @param messageFromClient    - строка, никнейм текущего игрока.
         * @param currentPlayer        - поток, текущий игрок.
         * @param anotherMessagePrefix - строка, префикс.
         * @param anotherPlayer        - поток, второй игрок.
         */
        private void fixNickname(String messageFromClient,
                                 ClientThread currentPlayer,
                                 String anotherMessagePrefix,
                                 ClientThread anotherPlayer) {
            currentPlayer.playerNickname = messageFromClient.substring(PREFIX_FOR_NICKNAME.length());
            System.out.println(anotherMessagePrefix + messageFromClient);
            anotherPlayer.sendMessage(messageFromClient);
        }

        /**
         * Метод, отправляет сообщение клиенту.
         *
         * @param message - строка, сообщение.
         */
        public void sendMessage(String message) {
            toClient.println(message);
        }

        /**
         * Метод, проверяет являестя ли игрок первым игроком.
         *
         * @return - true, если ссылки равны.
         */
        private boolean isFirst() {
            return this == firstPlayer;
        }
    }
}
