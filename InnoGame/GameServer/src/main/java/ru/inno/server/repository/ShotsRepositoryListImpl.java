package ru.inno.server.repository;

import ru.inno.model.models.Shot;

import java.util.ArrayList;
import java.util.List;

/**
 *27.03.2021 12:11
 *InnoGame
 *
 *@author Papsuev Timofey
 *@version v1.0
 */
public class ShotsRepositoryListImpl implements ShotsRepository {
    /**
     * Список хранит объекты типа Shot.
     */
    private List<Shot> shots;

    public ShotsRepositoryListImpl() {
        this.shots = new ArrayList<>();
    }

    @Override
    public void save(Shot shot) {
        shot.setIdShot((long)shots.size());
        shots.add(shot);
    }
}
