package ru.inno.server.algorithms;

import java.io.*;

/**
 * 07.04.2021 16:08
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class GeneratorId {
    /**
     * Метод генерирует последовательный номер id для объекта вызывающего этот метод.
     *
     * @return - число типа Long.
     */
    public static Long generateId(String fileName) {
        /**
         * Число, последний сгенерированный id.
         */
        long id;

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String lastGeneratedIdAsString = reader.readLine();     // прочитали последний сгенерированный id как строку

            if (lastGeneratedIdAsString == null) {
                lastGeneratedIdAsString = "0";
            }
            id = Long.parseLong(lastGeneratedIdAsString);      // преобразуем ее в Long
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(String.valueOf(id + 1));
            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
