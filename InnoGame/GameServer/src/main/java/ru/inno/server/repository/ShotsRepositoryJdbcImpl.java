package ru.inno.server.repository;

import ru.inno.model.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

/**
 * 14.04.2021 22:26
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class ShotsRepositoryJdbcImpl implements ShotsRepository {
    /**
     * Ссылка, на объект типа DataSource для подключения к базе данных.
     */
    private final DataSource dataSource;

    /**
     * Запрос к БД на запись новой строки в БД.
     */
    private static final String SQL_INSERT_SHOT = "insert into shot (" +
            "shot_time, " +
            "current_id_game, " +
            "player_shot, " +
            "player_target) " +
            "values (?, ?, ?, ?)";

    public ShotsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SHOT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setTimestamp(1, Timestamp.valueOf(shot.getShotTime()));
            statement.setLong(2, shot.getCurrentIdGame().getIdGame());
            statement.setString(3, shot.getPlayerShot().getNamePlayer());
            statement.setString(4, shot.getPlayerTarget().getNamePlayer());

            int affectedRow = statement.executeUpdate();

            if (affectedRow != 1) {
                throw new SQLException("Can't insert data");
            }
            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    shot.setIdShot(generatedId.getLong("id_shot"));
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        //TODO: отладочный вывод, удалить
        System.out.println("сохранение данных по выстрелу c id - " + shot.getIdShot());
        //TODO: отладочный вывод, удалить
        System.out.println(shot);
    }
}

