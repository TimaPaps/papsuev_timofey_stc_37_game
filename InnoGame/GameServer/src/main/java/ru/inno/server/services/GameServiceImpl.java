package ru.inno.server.services;

import ru.inno.server.dto.StatisticDto;
import ru.inno.model.models.Game;
import ru.inno.model.models.Player;
import ru.inno.model.models.Shot;
import ru.inno.server.repository.GamesRepository;
import ru.inno.server.repository.PlayersRepository;
import ru.inno.server.repository.ShotsRepository;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * 26.03.2021 14:40
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
// бизнес - логика
public class GameServiceImpl implements GameService {
    /**
     * Ссылка на объект типа PlayersRepository
     */
    private final PlayersRepository playersRepository;
    /**
     * Ссылка на объект типа GamesRepository
     */
    private final GamesRepository gamesRepository;
    /**
     * Ссылка на объект типа ShotsRepository
     */
    private final ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили информацию об игроках
        Player firstPlayer = checkIfExists(firstIp, firstPlayerNickname);
        Player secondPlayer = checkIfExists(secondIp, secondPlayerNickname);

        // создали игру
        Game game = Game.builder()
                .dateTime(LocalDateTime.now())
                .firstPlayer(firstPlayer)
                .secondPlayer(secondPlayer)
                .countShotsFirstPlayer(0)
                .countShotsSecondPlayer(0)
                .secondsTotalTimeGame(0L)
                .build();
        // сохранили игру в репозитории
        gamesRepository.save(game);
        return game.getIdGame();
    }

    /**
     * Метод проверяет есть ли запись в репозитории на игрока, если есть, то обновляет IP-адрес,
     * если нет, то создает нового игрока и сохраняет экземпляр в репозитории.
     *
     * @param ip       - IP-адрес игрока.
     * @param nickname - никнейм игрока.
     */
    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет игрока под таким никнеймом
        if (player == null) {
            player = Player.builder()
                    .ipLastConnect(ip)
                    .namePlayer(nickname)
                    .maxPoints(0)
                    .numbersOfWins(0)
                    .numbersOfDefeats(0)
                    .build();

            playersRepository.save(player);                                                     // сохраняем его в репозитории
        } else {
            player.setIpLastConnect(ip);                                                            // если такой игрок был - обновляем у него IP-адрес
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long idGame, String playerShotNickname, String playerTargetNickname) {
        Player playerShot = playersRepository.findByNickname(playerShotNickname);       // получаем из репозитория того, кто стрелял
        Player playerTarget = playersRepository.findByNickname(playerTargetNickname);   // получаем из репозитория того, в кого стреляли
        Game game = gamesRepository.findById(idGame);   // получаем игру
        Shot shot = Shot.builder()
                .shotTime(LocalDateTime.now())
                .currentIdGame(game)
                .playerShot(playerShot)
                .playerTarget(playerTarget)
                .build();

        // увеличиваем очки у стреляющего при попадании в цель
        playerShot.setMaxPoints(playerShot.getMaxPoints() + 1);

        //если стрелявший первый игрок
        if (game.getFirstPlayer().getNamePlayer().equals(playerShotNickname)) {
            // сохраняем информацию (общее количество выстрелов) о выстреле в игре
            game.setCountShotsFirstPlayer(game.getCountShotsFirstPlayer() + 1);
            game.getFirstPlayer().setMaxPoints(game.getFirstPlayer().getMaxPoints() + 1);
        }

        //если стрелявший второй игрок
        if (game.getSecondPlayer().getNamePlayer().equals(playerShotNickname)) {
            // сохраняем информацию (общее количество выстрелов) о выстреле в игре
            game.setCountShotsSecondPlayer(game.getCountShotsSecondPlayer() + 1);
            game.getSecondPlayer().setMaxPoints(game.getSecondPlayer().getMaxPoints() + 1);
        }

        playersRepository.update(playerShot);   // обновляем данные по стреляющему
        gamesRepository.update(game);           // обновляем данные по игре
        shotsRepository.save(shot);             //сохраняем выстрел
    }

    @Override
    public StatisticDto finishGame(Long idGame) {
        Game game = gamesRepository.findById(idGame);   // получаем игру

        // если количество попавших в цель выстрелов первого игрока больше чем у второго
        if (game.getCountShotsFirstPlayer() > game.getCountShotsSecondPlayer()) {
            game.getFirstPlayer().setNumbersOfWins(game.getFirstPlayer().getNumbersOfWins() + 1);           // увеличиваем количество побед у первого игрока
            game.getSecondPlayer().setNumbersOfDefeats(game.getSecondPlayer().getNumbersOfDefeats() + 1);   // увеличиваем количество поражений у второго игрока
        }

        // если количество попавших в цель выстрелов первого игрока меньше чем у второго
        if (game.getCountShotsFirstPlayer() < game.getCountShotsSecondPlayer()) {
            game.getFirstPlayer().setNumbersOfDefeats(game.getFirstPlayer().getNumbersOfDefeats() + 1);    // увеличиваем количество поражений у первого игрока
            game.getSecondPlayer().setNumbersOfWins(game.getSecondPlayer().getNumbersOfWins() + 1);        // увеличиваем количество побед у второго игрока
        }

        playersRepository.update(game.getFirstPlayer());                    // обновляем данные по первому игроку
        playersRepository.update(game.getSecondPlayer());                   // обновляем данные по второму игроку

        game.setSecondsTotalTimeGame(totalTimeGame(game.getDateTime()));    // устанавливаем время продолжительности игры
        gamesRepository.update(game);                                       // обновляем данные по игре
        return new StatisticDto(game);                                      // возвращаем новый объект статистики по игре
    }

    /**
     * Метод возвращает время продолжительности игры в секундах.
     *
     * @param localDateTime - дата и время начала игры.
     * @return - число, время продолжительности игры в секундах.
     */
    private Long totalTimeGame(LocalDateTime localDateTime) {
        LocalDateTime timeFinishGame = LocalDateTime.now();                     // получаем время окончания игры
        Duration duration = Duration.between(localDateTime, timeFinishGame);    // получаем разницу между началом и окончанием игры
        return duration.getSeconds();                                           // возвращаем разницу в секундах
    }
}
