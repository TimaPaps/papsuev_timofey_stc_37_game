package ru.inno.server.repository;

import ru.inno.model.models.Game;
import ru.inno.model.models.Player;
import ru.inno.model.models.Shot;

import java.time.LocalDateTime;

/**
 * 21.05.2021 20:13
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class GeneratorTestObjects {
    public Player playerWithOutId(String ipLastConnect, String namePlayer) {
        return Player.builder()
                .ipLastConnect(ipLastConnect)
                .namePlayer(namePlayer)
                .maxPoints(0)
                .numbersOfWins(0)
                .numbersOfDefeats(0)
                .build();
    }

    public Player playerWithId(Long idPlayer, String ipLastConnect, String namePlayer) {
        return Player.builder()
                .idPlayer(idPlayer)
                .ipLastConnect(ipLastConnect)
                .namePlayer(namePlayer)
                .maxPoints(0)
                .numbersOfWins(0)
                .numbersOfDefeats(0)
                .build();
    }

    public Player playerWithIdForUpdate(long idPlayer, String ipLastConnect, String namePlayer, int maxPoints, int numbersOfWins, int numbersOfDefeats) {
        return Player.builder()
                .idPlayer(idPlayer)
                .ipLastConnect(ipLastConnect)
                .namePlayer(namePlayer)
                .maxPoints(maxPoints)
                .numbersOfWins(numbersOfWins)
                .numbersOfDefeats(numbersOfDefeats)
                .build();
    }

    public Game gameWithOutId(Player firstPlayer, Player secondPlayer) {
        return Game.builder()
                .dateTime(LocalDateTime.now())
                .firstPlayer(firstPlayer)
                .secondPlayer(secondPlayer)
                .countShotsFirstPlayer(0)
                .countShotsSecondPlayer(0)
                .secondsTotalTimeGame(0L)
                .build();
    }

    public Game gameWithId(Player firstPlayer, Player secondPlayer) {
        return Game.builder()
                .idGame(1L)
                .dateTime(LocalDateTime.now())
                .firstPlayer(firstPlayer)
                .secondPlayer(secondPlayer)
                .countShotsFirstPlayer(0)
                .countShotsSecondPlayer(0)
                .secondsTotalTimeGame(0L)
                .build();
    }

    public Game gameWithIdForUpdate(Player firstPlayer, Player secondPlayer, int countShotsFirstPlayer, int countShotsSecondPlayer, long secondsTotalTimeGame) {
        return Game.builder()
                .idGame(1L)
                .dateTime(LocalDateTime.now())
                .firstPlayer(firstPlayer)
                .secondPlayer(secondPlayer)
                .countShotsFirstPlayer(countShotsFirstPlayer)
                .countShotsSecondPlayer(countShotsSecondPlayer)
                .secondsTotalTimeGame(secondsTotalTimeGame)
                .build();
    }

    public Shot shot(Game game, Player firstPlayer, Player secondPlayer) {
        return Shot.builder()
                .shotTime(LocalDateTime.now())
                .currentIdGame(game)
                .playerShot(firstPlayer)
                .playerTarget(secondPlayer)
                .build();
    }
}
