package ru.inno.server.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ru.inno.model.models.Player;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * 21.05.2021 17:58
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName("Test for PlayersRepositoryJdbc")
@ExtendWith(MockitoExtension.class)
class PlayersRepositoryJdbcImplTest {

    @Mock
    private Player player;

    private PlayersRepositoryJdbcImpl playersRepositoryJdbc;

    @Nested
    @DisplayName("save() is working if:")
    class ForSave {
        @BeforeEach
        public void setUp() {
            createH2Database();
            generatorTestObject();
        }

        @Test
        void save_player_to_the_database() {
            playersRepositoryJdbc.save(player);
        }
    }

    @Nested
    @DisplayName("findById() is working if:")
    class ForFindByNickname {
        @BeforeEach
        public void setUp() {
            createH2Database();
        }

        @ParameterizedTest
        @ValueSource(strings = {"Tima", "Dima"})
        void find_the_player_by_nickname_in_the_database(String nickname) {
            assertNotNull(playersRepositoryJdbc.findByNickname(nickname));
        }
    }

    @Nested
    @DisplayName("update() is working if:")
    class ForUpdate {
        @BeforeEach
        public void setUp() {
            createH2Database();
            generatorTestObjectForUpdate();
        }

        @Test
        void update_player_to_the_database() {
            playersRepositoryJdbc.update(player);
        }
    }

    private void createH2Database() {
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("sql/testPlayersRepositoryJdbc/schemaH2.sql")
                .addScript("sql/testPlayersRepositoryJdbc/dataH2.sql")
                .build();
        playersRepositoryJdbc = new PlayersRepositoryJdbcImpl(dataSource);
    }

    private void generatorTestObject() {
        player = new GeneratorTestObjects().playerWithOutId("127.0.0.1", "Miha");
    }

    private void generatorTestObjectForUpdate() {
        player = new GeneratorTestObjects().playerWithIdForUpdate(1L, "127.0.0.11", "Tima", 55, 33, 4);
    }
}
