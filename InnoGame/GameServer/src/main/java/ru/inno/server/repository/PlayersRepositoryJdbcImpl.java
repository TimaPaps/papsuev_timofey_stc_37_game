package ru.inno.server.repository;

import ru.inno.model.models.Player;

import javax.sql.DataSource;
import java.sql.*;

/**
 * 14.04.2021 21:13
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    /**
     * Ссылка, на объект типа DataSource для подключения к базе данных.
     */
    private final DataSource dataSource;

    /**
     * Запрос к БД на запись новой строки в БД.
     */
    private static final String SQL_INSERT_PLAYER = "insert into player (" +
            "ip_last_connect, " +
            "name_player, max_points, " +
            "numbers_of_wins, " +
            "numbers_of_defeats) " +
            "values (?, ?, ?, ?, ?)";

    /**
     * Запрос к БД на поиск строки с определенной строкой (никнейм игрока).
     */
    private static final String SQL_FIND_PLAYER_BY_NICKNAME = "select * from player where name_player = ?";

    /**
     * Запрос к БД на обновление данных в строке с определенным ID в БД.
     */
    private static final String SQL_UPDATE_PLAYER_BY_ID = "update player set " +
            "ip_last_connect = ?, " +
            "name_player = ?, " +
            "max_points = ?, " +
            "numbers_of_wins = ?, " +
            "numbers_of_defeats = ? " +
            "where id_player = ?";

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, player.getIpLastConnect());
            statement.setString(2, player.getNamePlayer());
            statement.setInt(3, player.getMaxPoints());
            statement.setInt(4, player.getNumbersOfWins());
            statement.setInt(5, player.getNumbersOfDefeats());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    player.setIdPlayer(generatedId.getLong("id_player"));
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        //TODO: отладочный вывод, удалить
        System.out.println("сохранение данных по игроку c id - " + player.getIdPlayer());
        //TODO: отладочный вывод, удалить
        System.out.println(player);
    }

    /**
     * Метод, преобразует строку в объект типа Player.
     *
     * @param prefix - строка, префикс.
     * @return - обьект типа Player.
     */
    protected static RowMapper<Player> playerRowMapper(String prefix) {
        return row -> Player.builder()
                .idPlayer(row.getLong(prefix + "id_player"))
                .ipLastConnect(row.getString(prefix + "ip_last_connect"))
                .namePlayer(row.getString(prefix + "name_player"))
                .maxPoints(row.getInt(prefix + "max_points"))
                .numbersOfWins(row.getInt(prefix + "numbers_of_wins"))
                .numbersOfDefeats(row.getInt(prefix + "numbers_of_defeats"))
                .build();
    }

    @Override
    public Player findByNickname(String nickname) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_NICKNAME)) {
            statement.setString(1, nickname);

            try (ResultSet row = statement.executeQuery()) {
                if (row.next()) {
                    //TODO: отладочный вывод, удалить
                    System.out.println("нашли игрока по никнейму - " + nickname);
                    //TODO: отладочный вывод, удалить
                    System.out.println(playerRowMapper("").mapRow(row));
                    return playerRowMapper("").mapRow(row);
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }

    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PLAYER_BY_ID)) {

            statement.setString(1, player.getIpLastConnect());
            statement.setString(2, player.getNamePlayer());
            statement.setInt(3, player.getMaxPoints());
            statement.setInt(4, player.getNumbersOfWins());
            statement.setInt(5, player.getNumbersOfDefeats());
            statement.setLong(6, player.getIdPlayer());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        //TODO: отладочный вывод, удалить
        System.out.println("обновление данных по игроку c id - " + player.getIdPlayer());
        //TODO: отладочный вывод, удалить
        System.out.println(player);
    }
}
