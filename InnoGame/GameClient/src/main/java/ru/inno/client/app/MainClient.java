package ru.inno.client.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.inno.client.controllers.MainController;

import java.util.Objects;

/**
 * 27.04.2021 12:53
 * 39.Socket IO - Client
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class MainClient extends Application {
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        // указываем путь к файлу main.fxml
        String fxmlFileName = "/fxml/main.fxml";
        // указываем путь к файлу стилей style.css
        String cssFileName = "/css/style.css";
        // создаем загрузчик fxml файла
        FXMLLoader loader = new FXMLLoader();
        // получаем для текущего класса main его ресурсы - fxml - и передаем его в загрузчик
        Parent root = loader.load(getClass().getResourceAsStream(fxmlFileName));
        // задаем имя для primaryStage
        primaryStage.setTitle("Game Client"); // класс Stage - главный JavaFX container
        // создаем сцену
        Scene scene = new Scene(root);
        // кладем в primaryStage сцену
        primaryStage.setScene(scene);
        // задаем фиксюразмер для окна приложения
        primaryStage.setResizable(false);
        // подключение файла со стилями
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource(cssFileName)).toExternalForm());

        // получаем MainController
        MainController controller = loader.getController();
        // получаем у контроллера перехватчик событий (обработчик нажатий) и добавляем его в сцену
        scene.setOnKeyPressed(controller.getKeyEventEventHandler());

        // визуализируем окно
        primaryStage.show();
    }
}
