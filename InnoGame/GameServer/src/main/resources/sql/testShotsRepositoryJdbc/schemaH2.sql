drop table if exists shot;

create table shot
(
    id_shot         long auto_increment primary key,
    shot_time       timestamp,
    current_id_game long not null,
    player_shot     varchar(50) not null,
    player_target   varchar(50) not null
);
