package ru.inno.server.appconsole;

/**
 * 23.03.2021 16:34
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ShowGame show = new ShowGame();
        show.showGame();
    }
}
