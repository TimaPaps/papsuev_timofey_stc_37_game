package ru.inno.client.constants;

/**
 * 06.05.2021 0:58
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Constants {
    /**
     * Строка, константа префикс для никнейма.
     */
    public static final String PREFIX_FOR_NICKNAME = "name: ";
    /**
     * Строка, константа для выхода из игры.
     */
    public static final String EXIT = "exit";
    /**
     * Строка, константа шаг влево.
     */
    public static final String STEP_LEFT = "left";
    /**
     * Строка, константа шаг вправо.
     */
    public static final String STEP_RIGHT = "right";
    /**
     * Строка, константа выстрел.
     */
    public static final String SHOT = "shot";
    /**
     * Строка, константа игроки подключены.
     */
    public static final String PLAYERS_CONNECTED = "Второй игрок подключен!";
    /**
     * Строка, константа старт игры.
     */
    public static final String GAME_IS_STARTED = "game is started";
    /**
     * Число, константа шаг перемещения игрока.
     */
    public static final int PLAYER_STEP = 10;
    /**
     * Число, константа значение, на которое уменьшится показатель состояния брони.
     */
    public static final int DAMAGE = 10;
    /**
     * Строка, константа попадание пули в цель.
     */
    public static final String MESSAGE_DAMAGE = "damage";
    /**
     * Строка, константа для вывода статистики по игре.
     */
    public static final String PREFIX_FOR_STATISTIC = "statistic: ";
}
