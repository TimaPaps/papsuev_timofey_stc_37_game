package ru.inno.server.appconsole;

import com.zaxxer.hikari.HikariDataSource;
import ru.inno.server.configuration.ConfigurationHikari;
import ru.inno.server.dto.StatisticDto;
import ru.inno.server.repository.*;
import ru.inno.server.services.GameService;
import ru.inno.server.services.GameServiceImpl;

import javax.sql.DataSource;
import java.util.Random;
import java.util.Scanner;

import static ru.inno.server.configuration.Configuration.*;

/**
 * 30.03.2021 13:19
 * InnoGame
 *
 * Класс демонстрирует работоспособность программы.
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class ShowGame {

    /**
     * Метод запускает игру и показывает статистику.
     */
    public void showGame() {

        DataSource dataSource = new HikariDataSource(ConfigurationHikari.getHikariConfig());

//        PlayersRepository playersRepository = new PlayersRepositoryMapImpl();
        PlayersRepository playersRepository = new PlayersRepositoryFileImpl(getDbFileNamePlayer(), getSequenceFileNamePlayer());
//        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        GamesRepository gamesRepository = new GamesRepositoryListImpl();
//        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
//        ShotsRepository shotsRepository = new ShotsRepositoryListImpl();
        ShotsRepository shotsRepository = new ShotsRepositoryFileImpl(getDbFileNameShot(), getSequenceFileNameShot());
//        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        Scanner scanner = new Scanner(System.in);

        int numberOfGames = 0;
        Long idGame = null;

        while (numberOfGames < 1) {
            System.out.println("Введите имена игроков, имена должны быть разными!");
            String firstPlayer = "";
            String secondPlayer = "";

            while (firstPlayer.isEmpty()) {
                firstPlayer = scanner.nextLine();

                if (firstPlayer.isEmpty()) {
                    System.err.println("Введите имя первого игрока");
                }
            }
            System.out.println("Введите имя второго игрока");

            while (secondPlayer.isEmpty()) {
                secondPlayer = scanner.nextLine();

                if (secondPlayer.isEmpty()) {
                    System.err.println("Введите имя второго игрока");
                }
            }

            System.out.println();
            Random random = new Random();

            if (!firstPlayer.equals(secondPlayer)) {
                idGame = gameService.startGame("111", "222", firstPlayer, secondPlayer);
            } else {
                System.err.println("У игроков должны быть разные имена");
                continue;
            }
            String shooter = firstPlayer;
            String target = secondPlayer;
            int numbersOfAttacks = 0;

            while (numbersOfAttacks < 4) {
                System.out.println(shooter + " делайте выстрел в " + target);
                scanner.nextLine();
                int success = random.nextInt(2);

                if (success == 0) {
                    System.out.println("Успешно!");
                    System.out.println();
                    gameService.shot(idGame, shooter, target);
                } else {
                    System.out.println("Промах!");
                    System.out.println();
                }
                String temp = shooter;
                shooter = target;
                target = temp;
                numbersOfAttacks++;
            }
            numbersOfAttacks = 0;
            numberOfGames++;
        }

        StatisticDto statistic = gameService.finishGame(idGame);
        System.out.println(statistic);
        scanner.close();
    }
}
