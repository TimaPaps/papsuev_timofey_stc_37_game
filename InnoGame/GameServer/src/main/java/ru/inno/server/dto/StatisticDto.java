package ru.inno.server.dto;

import ru.inno.model.models.Game;

/**
 * 27.03.2021 18:40
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
// информация об игре
public class StatisticDto {
    /**
     * Строка, константа.
     */
    private static final String PREFIX_STATISTIC = "statistic: ";
    /**
     * Ссылка на объект типа Game.
     */
    private final Game game;

    public StatisticDto(Game game) {
        this.game = game;
    }

    /**
     * Метод получает id объекта (игры).
     *
     * @return - число, id игры.
     */
    public Long getIdGame() {
        return game.getIdGame();
    }

    /**
     * Метод получает имя первого игрока.
     *
     * @return - строку, имя первого игрока.
     */
    public String getFirstPlayerName() {
        return game.getFirstPlayer().getNamePlayer();
    }

    /**
     * Метод получает имя второго игрока.
     *
     * @return - строку, имя второго игрока.
     */
    public String getSecondPlayerName() {
        return game.getSecondPlayer().getNamePlayer();
    }

    /**
     * Метод получает количество попаданий в цель первого игрока.
     *
     * @return - число, количество попаданий в цель первого игрока.
     */
    public Integer getCountShotsFirstPlayer() {
        return game.getCountShotsFirstPlayer();
    }

    /**
     * Метод получает количество попаданий в цель второго игрока.
     *
     * @return - число, количество попаданий в цель второго игрока.
     */
    public Integer getCountShotsSecondPlayer() {
        return game.getCountShotsSecondPlayer();
    }

    /**
     * Метод получает максимальное количество очков первого игрока.
     *
     * @return - число, максимальное количество очков первого игрока.
     */
    public Integer getMaxPointsFirstPlayer() {
        return game.getFirstPlayer().getMaxPoints();
    }

    /**
     * Метод получает максимальное количество очков второго игрока.
     *
     * @return - число, максимальное количество очков второго игрока.
     */
    public Integer getMaxPointsSecondPlayer() {
        return game.getSecondPlayer().getMaxPoints();
    }

    /**
     * Метод получает продолжительность игры в секундах.
     *
     * @return - число, продолжительность игры в секундах.
     */
    public Long getSecondTotalTimeGame() {
        return game.getSecondsTotalTimeGame();
    }

    /**
     * Метод получает победителя в игре, победитель - игрок у которого больше попаданий в цель,
     * если количество попаданий в цель у игроков равны, то победитель не устанавливается.
     *
     * @return - строка, имя победителя, либо строка "ничья!".
     */
    public String getWinner() {
        /**
         * Строка, победитель в игре.
         */
        String winner;

        if (getCountShotsFirstPlayer() > getCountShotsSecondPlayer()) {
            winner = getFirstPlayerName();
        } else if (getCountShotsFirstPlayer() < getCountShotsSecondPlayer()) {
            winner = getSecondPlayerName();
        } else {
            winner = "ничья!";
        }
        return winner;
    }

    @Override
    public String toString() {
        return PREFIX_STATISTIC + "ИГРА с ID = " + getIdGame() + "; "
                + " ИГРОК: " + getFirstPlayerName() + ", попаданий - " + getCountShotsFirstPlayer()
                + ", всего очков - " + getMaxPointsFirstPlayer() + "; "
                + " ИГРОК: " + getSecondPlayerName() + ", попаданий - " + getCountShotsSecondPlayer()
                + ", всего очков - " + getMaxPointsSecondPlayer() + "; "
                + " ПОБЕДА: " + getWinner() + "; "
                + " ИГРА ДЛИЛАСЬ: " + getSecondTotalTimeGame() + " секунд";
    }
}
