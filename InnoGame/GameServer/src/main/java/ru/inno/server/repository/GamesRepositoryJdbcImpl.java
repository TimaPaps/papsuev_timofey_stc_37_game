package ru.inno.server.repository;

import ru.inno.model.models.Game;

import javax.sql.DataSource;
import java.sql.*;

import static ru.inno.server.repository.PlayersRepositoryJdbcImpl.playerRowMapper;

/**
 * 14.04.2021 22:31
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class GamesRepositoryJdbcImpl implements GamesRepository {

    /**
     * Ссылка на объект типа DataSource для подключения к базе данных.
     */
    private final DataSource dataSource;

    /**
     * Запрос к БД на запись новой строки в БД.
     */
    private static final String SQL_INSERT_GAME = "insert into game (" +
            "date_time, " +
            "first_player, " +
            "second_player, " +
            "count_shots_first_player, " +
            "count_shots_second_player, " +
            "seconds_total_time_game) " +
            "values (?, ?, ?, ?, ?, ?)";

    /**
     * Запрос к БД на получение строки по определенному ID игры с выборкой данных из присоединенных таблиц
     * на игроков текущей игры.
     */
    private static final String SQL_FIND_GAME_BY_ID = "select " +
            "id_game, " +
            "date_time, " +
            "first_player, " +
            "second_player, " +
            "count_shots_first_player, " +
            "count_shots_second_player, " +
            "seconds_total_time_game, " +
            "p1.id_player as p1_id_player, " +
            "p1.ip_last_connect as p1_ip_last_connect, " +
            "p1.name_player as p1_name_player, " +
            "p1.max_points as p1_max_points, " +
            "p1.numbers_of_wins as p1_numbers_of_wins, " +
            "p1.numbers_of_defeats as p1_numbers_of_defeats, " +
            "p2.id_player as p2_id_player, " +
            "p2.ip_last_connect as p2_ip_last_connect, " +
            "p2.name_player as p2_name_player, " +
            "p2.max_points as p2_max_points, " +
            "p2.numbers_of_wins as p2_numbers_of_wins, " +
            "p2.numbers_of_defeats as p2_numbers_of_defeats " +
            "from game " +
            "left join player p1 on game.first_player = p1.name_player " +
            "left join player p2 on game.second_player = p2.name_player " +
            "where id_game = ?";

    /**
     * Запрос к БД на обновление данных в строке с определенным ID в БД.
     */
    private static final String SQL_UPDATE_GAME_BY_ID = "update game set " +
            "date_time = ?, " +
            "first_player = ?, " +
            "second_player = ?, " +
            "count_shots_first_player = ?, " +
            "count_shots_second_player = ?, " +
            "seconds_total_time_game = ? " +
            "where id_game = ?";

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {

            statement.setTimestamp(1, Timestamp.valueOf(game.getDateTime()));
            statement.setString(2, game.getFirstPlayer().getNamePlayer());
            statement.setString(3, game.getSecondPlayer().getNamePlayer());
            statement.setInt(4, game.getCountShotsFirstPlayer());
            statement.setInt(5, game.getCountShotsSecondPlayer());
            statement.setLong(6, game.getSecondsTotalTimeGame());

            int affectedRow = statement.executeUpdate();

            if (affectedRow != 1) {
                throw new SQLException("Can't insert data");
            }
            try (ResultSet generatedID = statement.getGeneratedKeys()) {
                if (generatedID.next()) {
                    game.setIdGame(generatedID.getLong("id_game"));
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        //TODO: отладочный вывод, удалить
        System.out.println("сохранение данных по игре c id - " + game.getIdGame());
        //TODO: отладочный вывод, удалить
        System.out.println(game);
    }

    /**
     * Класс, преобразует строку в объект типа Game.
     */
    private final RowMapper<Game> gameRowMapper = row -> Game.builder()
            .idGame(row.getLong("id_game"))
            .dateTime(row.getTimestamp("date_time").toLocalDateTime())
            .firstPlayer(playerRowMapper("p1_").mapRow(row))
            .secondPlayer(playerRowMapper("p2_").mapRow(row))
            .countShotsFirstPlayer(row.getInt("count_shots_first_player"))
            .countShotsSecondPlayer(row.getInt("count_shots_second_player"))
            .secondsTotalTimeGame(row.getLong("seconds_total_time_game"))
            .build();

    public Game findById(Long idGame) {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_GAME_BY_ID)) {
            statement.setLong(1, idGame);
            try (ResultSet row = statement.executeQuery()) {
                if (row.next()) {
                    //TODO: отладочный вывод, удалить
                    System.out.println("нашли игру по id - " + idGame);
                    //TODO: отладочный вывод, удалить
                    System.out.println(gameRowMapper.mapRow(row));
                    return gameRowMapper.mapRow(row);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }

    @Override
    public void update(Game game) {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME_BY_ID)) {

            statement.setTimestamp(1, Timestamp.valueOf(game.getDateTime()));
            statement.setString(2, game.getFirstPlayer().getNamePlayer());
            statement.setString(3, game.getSecondPlayer().getNamePlayer());
            statement.setInt(4, game.getCountShotsFirstPlayer());
            statement.setInt(5, game.getCountShotsSecondPlayer());
            statement.setLong(6, game.getSecondsTotalTimeGame());
            statement.setLong(7, game.getIdGame());

            int affectedRow = statement.executeUpdate();

            if (affectedRow != 1) {
                throw new SQLException("Can't update data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        //TODO: отладочный вывод, удалить
        System.out.println("обновление данных по игре с id - " + game.getIdGame());
        //TODO: отладочный вывод, удалить
        System.out.println(game);
    }
}
