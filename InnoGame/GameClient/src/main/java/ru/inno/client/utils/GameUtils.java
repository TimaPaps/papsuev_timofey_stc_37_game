package ru.inno.client.utils;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import ru.inno.client.controllers.MainController;
import ru.inno.client.socket.SocketClient;

import java.util.Objects;

import static ru.inno.client.constants.Constants.*;

/**
 * 03.05.2021 1:12
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class GameUtils {
    /**
     * Ссылка на объект типа AnchorPane.
     */
    private AnchorPane anchorPane;
    /**
     * Ссылка на объект типа MainController.
     */
    private MainController mainController;
    /**
     * сылка на объект типа SocketClient.
     */
    private SocketClient socketClient;

    public void setAnchorPane(AnchorPane anchorPane) {
        this.anchorPane = anchorPane;
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setSocketClient(SocketClient socketClient) {
        this.socketClient = socketClient;
    }

    /**
     * Метод, перемещает координату Х вправо (движение игрока вправо).
     *
     * @param fxPlayer - объект типа ImageView.
     */
    public void goImgRight(ImageView fxPlayer) {
        fxPlayer.setX(fxPlayer.getX() + PLAYER_STEP);
    }

    /**
     * Метод, перемещает координату Х влево (движение игрока влево).
     *
     * @param fxPlayer - объект типа ImageView.
     */
    public void goImgLeft(ImageView fxPlayer) {
        fxPlayer.setX(fxPlayer.getX() - PLAYER_STEP);
    }

    /**
     * Метод, создает пулю для игрока, если isEnemy - false, то выстрел произвел игрок стрелок,
     * если isEnemy - true, то выстрел произвел игрок цель.
     *
     * @param fxPlayer - объект типа ImageView (игрок).
     * @param isEnemy  - булева, по умолчанию - false.
     * @return - объект типа ImageView (пуля выстрелившего игрока).
     */
    public ImageView createImgBulletFor(ImageView fxPlayer, boolean isEnemy) {
        /**
         * Объект типа ImageView (пуля).
         */
        final ImageView bullet;
        /**
         * Число, шаг анимации.
         */
        final int value;
        /**
         * Объект типа ImageView (цель - противник).
         */
        final ImageView target;
        /**
         * Объект типа Pane (состояние брони).
         */
        final Pane targetHealth;
        /**
         * Объект типа ImageView (картинка для проигравшего).
         */
        final ImageView fail = mainController.getFxFail();
        // скрываем картинку для проигравшего
        fail.setVisible(false);

        // создаем картинки пули для игрока и противника
        if (!isEnemy) {
            bullet = new ImageView(Objects.requireNonNull(getClass()
                    .getResource("/images/playerBullet.png"))
                    .toExternalForm());
        } else {
            bullet = new ImageView(Objects.requireNonNull(getClass()
                    .getResource("/images/enemyBullet.png"))
                    .toExternalForm());
        }
        // задаем размеры пули
        bullet.setFitWidth(6);
        bullet.setFitHeight(12);

        // помещаем потомкам панели приложения этот объект (пулю)
        anchorPane.getChildren().add(bullet);
        // устанавливаем для пули положение на панели по Х-у относительно игрока стрелка
        bullet.setX(fxPlayer.getX() + fxPlayer.getLayoutX() + (fxPlayer.getFitWidth() / 2) - (bullet.getFitWidth() / 2));

        // устанавливаем для пули положение на панели по У-у относительно игрока стрелка
        if (!isEnemy) {
            bullet.setY(fxPlayer.getY() + fxPlayer.getLayoutY() - bullet.getFitWidth());
        } else {
            bullet.setY(fxPlayer.getY() + fxPlayer.getLayoutY() + fxPlayer.getFitHeight() - bullet.getFitHeight() / 2);
        }

        // направление анимации - противник стреляет в игрока или игрок стреляет в противника
        if (isEnemy) {
            value = 1;
        } else {
            value = -1;
        }

        // определяем, в кого произведен выстрел
        if (!isEnemy) {
            target = mainController.getFxEnemy();
            targetHealth = mainController.getFxHealthEnemy();
        } else {
            target = mainController.getFxPlayer();
            targetHealth = mainController.getFxHealthPlayer();
        }
        // анимация полета пули
        animationForBullet(isEnemy, bullet, value, target, targetHealth, fail);
        return bullet;
    }

    /**
     * Метод, анимация полета пули и определение завершения игры.
     *
     * @param isEnemy      - булева, по умолчанию - false.
     * @param bullet       - объект типа ImageView.
     * @param value        - число, шаг анимации.
     * @param target       - объект типа ImageView (цель - противник).
     * @param targetHealth - объект типа Pane (состояние брони).
     * @param fail         - объект типа ImageView (картинка для проигравшего).
     */
    private void animationForBullet(boolean isEnemy, ImageView bullet, int value, ImageView target, Pane targetHealth, ImageView fail) {
        // создание анимации
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.0016), animation -> { // передаем в KeyFrame задержку и анимацию
            bullet.setY(bullet.getY() + value);

            // если пуля еще видна и произошло пересечение
            if (bullet.isVisible() && isImgIntersects(bullet, target)) {
                if (targetHealth.getWidth() > 0) {
                    // уменьшаем значение состояния брони
                    createDamage(targetHealth);
                } else {
                    // устанавливаем координаты для картинки для проигравшего
                    fail.setX(target.getX() + target.getLayoutX());
                    fail.setY(target.getY() + target.getLayoutY());
                    // показываем картинку для проигравшего
                    fail.setVisible(true);
                    // скрываем изначальную картинку для проигравшего
                    target.setVisible(false);
                    // отправляем сообщение на сервер о завершении игры
                    socketClient.sendMessage(EXIT);
                }
                // скрываем пулю
                bullet.setVisible(false);
                // отправляем сообщение, если попали во врага
                if (!isEnemy) {
                    socketClient.sendMessage(MESSAGE_DAMAGE);
                }
            }
            //TODO: если у пули координата выше/ниже нужной, ее можно удалить из pane.getChildren

        }));
        // запускаем в цикле анимацию 500 раз (сстояние полета пули)
        timeline.setCycleCount(500);
        // стартуем анимацию
        timeline.play();
        timeline.setOnFinished((finishAnimation) -> {
            bullet.setVisible(false);
            // удаление пули
            anchorPane.getChildren().remove(bullet);
        });
    }

    /**
     * Метод, определяет наличие пересечения координат объектов - пули и одного из игроков.
     *
     * @param bullet - объект типа ImageView (пуля).
     * @param player - объект типа ImageView (игрок).
     * @return - true, если произошло пересечение координат объектов - пули и одного из игроков.
     */
    private boolean isImgIntersects(ImageView bullet, ImageView player) {
        return bullet.getBoundsInParent().intersects(player.getBoundsInParent());
    }

    /**
     * Метод, уменьшает значение показателя состояния брони.
     *
     * @param healthLabel - объект типа Pane, показатель состояния брони.
     */
    private void createDamage(Pane healthLabel) {
        double healthPlayer = healthLabel.getPrefWidth();
        healthLabel.setPrefWidth(healthPlayer - DAMAGE);
    }
}
