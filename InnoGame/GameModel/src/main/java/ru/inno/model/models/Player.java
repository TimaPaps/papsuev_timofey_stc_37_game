package ru.inno.model.models;

import lombok.Builder;
import lombok.Data;

/**
 * 23.03.2021 16:45
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
@Builder
public class Player {
    /**
     * Идентификатор игрока.
     */
    private Long idPlayer;
    /**
     * IP-адрес, с которого последний раз заходил игрок.
     */
    private String ipLastConnect;
    /**
     * Имя игрока.
     */
    private String namePlayer;
    /**
     * Максимальное количество очков.
     */
    private Integer maxPoints;
    /**
     * Количество побед.
     */
    private Integer numbersOfWins;
    /**
     * Количество поражений.
     */
    private Integer numbersOfDefeats;
}
