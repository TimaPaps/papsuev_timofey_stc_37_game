package ru.inno.client.socket;

import javafx.application.Platform;
import ru.inno.client.controllers.MainController;
import ru.inno.client.exceptions.ExceptionForClient;
import ru.inno.client.utils.GameUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static ru.inno.client.constants.Constants.*;

/**
 * 27.04.2021 16:43
 * 39.Socket IO - Client
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class SocketClient extends Thread {
    /**
     * Канал подключения.
     */
    private final Socket socket;
    /**
     * Стрим для отправки сообщений на сервер.
     */
    private final PrintWriter toServer;
    /**
     * Стрим для получения сообщений с сервера.
     */
    private final BufferedReader fromServer;
    /**
     * Флаг, игра в процессе, состояние по умолчанию - true.
     */
    private boolean isGameInProcess = true;
    /**
     * Ссылка на объект MainController.
     */
    private final MainController mainController;
    /**
     * Ссылка на объект GameUtils.
     */
    private final GameUtils gameUtils;

    public SocketClient(MainController mainController, String host, int port) {
        try {
            socket = new Socket(host, port);
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.mainController = mainController;
            this.gameUtils = mainController.getGameUtils();
        } catch (Exception e) {
            throw new ExceptionForClient("no connection to the server", e);
        }
    }

    /**
     * Метод, получает сообщения от сервера.
     */
    @Override
    public void run() {
        while (isGameInProcess) {
            String messageFromServer;
            try {
                messageFromServer = fromServer.readLine();

                if (messageFromServer != null && !messageFromServer.equals(EXIT)) {
                    // вывод статистики
                    if (messageFromServer.startsWith(PREFIX_FOR_STATISTIC)) {
                        System.out.println(messageFromServer.substring(PREFIX_FOR_STATISTIC.length()));
                        Platform.runLater(() -> mainController
                                .getFxMessages()
                                .setText(messageFromServer
                                        .substring(PREFIX_FOR_STATISTIC.length())));
                    } else {
                        System.out.println("ОТ СЕРВЕРА: " + messageFromServer);
                    }
                    switch (messageFromServer) {
                        case STEP_RIGHT:
                            // ограничение двидения вправо
                            if (mainController.getFxEnemy().getX() + mainController.getFxEnemy().getLayoutX() <
                                    mainController.getFxRightPane().getLayoutX() -
                                            mainController.getFxEnemy().getFitWidth() - 15) {
                                // вращение картинки в сторону перемещения
                                Platform.runLater(() ->  mainController.getFxEnemy().setRotate(-90));
                                Platform.runLater(() -> gameUtils.goImgRight(mainController.getFxEnemy()));
                            }
                            break;
                        case STEP_LEFT:
                            // ограничение двидения влево
                            if (mainController.getFxEnemy().getX() + mainController.getFxEnemy().getLayoutX() >
                                    mainController.getFxAnchorPane().getLayoutX() + 10) {
                                // вращение картинки в сторону перемещения
                                Platform.runLater(() ->  mainController.getFxEnemy().setRotate(90));
                                Platform.runLater(() -> gameUtils.goImgLeft(mainController.getFxEnemy()));
                            }
                            break;
                        case SHOT:
                            // вращение картинки в сторону противника
                            Platform.runLater(() ->  mainController.getFxEnemy().setRotate(0));
                            // Platform.runLater(() - сообщаем JavaFX чтобы она запускала в своем потоке приложение
                            // иначе ошибка - Not on FX application thread; currentThread = Thread-...
                            // так сказать чтобы подружить потоки JavaFX и Java
                            Platform.runLater(() -> gameUtils
                                    .createImgBulletFor(mainController.getFxEnemy(), true));
                            break;
                        case PLAYERS_CONNECTED:
                            Platform.runLater(() -> mainController
                                    .getFxLabelInfoGame()
                                    .setText("the second player is connected! enter your Name!"));

                            mainController.getFxTextPlayerNickName().setDisable(false);
                            mainController.getFxButtonStartGame().setDisable(false);
                            break;
                        case GAME_IS_STARTED:
                            Platform.runLater(() -> mainController.getFxLabelInfoGame().setText("game is started!"));
                            mainController.getFxButtonStopGame().setDisable(false);
                            break;
                    }
                } else {
                    Platform.runLater(() -> mainController.getFxLabelInfoGame().setText("game over!!!"));
                    System.out.println("Игра завершена!");
                    mainController.getFxButtonStopGame().setDisable(true);
                    isGameInProcess = false;
                    toServer.close();
                    fromServer.close();
                    socket.close();
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Метод, отправляет сообщение на сервер.
     *
     * @param message - строка, сообщение.
     */
    public void sendMessage(String message) {
        toServer.println(message);
    }
}
