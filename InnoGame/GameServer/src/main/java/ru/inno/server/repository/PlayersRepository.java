package ru.inno.server.repository;

import ru.inno.model.models.Player;

/**
 * 26.03.2021 14:44
 * InnoGame
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface PlayersRepository {
    /**
     * Метод сохраняет объект типа Player в репозитории.
     *
     * @param player - объект типа Player.
     */
    void save(Player player);

    /**
     * Метод осуществляет поиск объекта типа Player по его никнейму в репозитории.
     *
     * @param nickname - никнейм объекта (пользователя) типа Player.
     * @return - объект типа Player.
     */
     Player findByNickname(String nickname);

    /**
     * Метод обновляет данные объекта типа Player в репозитории.
     *
     * @param player - объект типа Player.
     */
    void update(Player player);
}
