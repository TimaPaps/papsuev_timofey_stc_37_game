insert into player (ip_last_connect,
                    name_player,
                    max_points,
                    numbers_of_wins,
                    numbers_of_defeats)
values ('127.0.0.11', 'Tima', 5, 3, 0);

insert into player (ip_last_connect,
                    name_player,
                    max_points,
                    numbers_of_wins,
                    numbers_of_defeats)
values ('127.0.0.22', 'Dima', 1, 0, 3);

insert into player (ip_last_connect,
                    name_player,
                    max_points,
                    numbers_of_wins,
                    numbers_of_defeats)
values ('127.0.0.33', 'Glasha', 15, 13, 3);

insert into player (ip_last_connect,
                    name_player,
                    max_points,
                    numbers_of_wins,
                    numbers_of_defeats)
values ('127.0.0.44', 'Masha', 25, 33, 17);

insert into player (ip_last_connect,
                    name_player,
                    max_points,
                    numbers_of_wins,
                    numbers_of_defeats)
values ('127.0.0.55', 'Vasya', 15, 13, 3);

insert into player (ip_last_connect,
                    name_player,
                    max_points,
                    numbers_of_wins,
                    numbers_of_defeats)
values ('127.0.0.66', 'Vova', 25, 33, 17);

insert into game (date_time,
                  first_player,
                  second_player,
                  count_shots_first_player,
                  count_shots_second_player,
                  seconds_total_time_game)
values ('2021-05-13 21:09:15.251000', 'Tima', 'Dima', 3, 5, 123);

insert into game (date_time,
                  first_player,
                  second_player,
                  count_shots_first_player,
                  count_shots_second_player,
                  seconds_total_time_game)
values ('2021-05-20 16:40:28.635000', 'Glasha', 'Masha', 45, 33, 323);
