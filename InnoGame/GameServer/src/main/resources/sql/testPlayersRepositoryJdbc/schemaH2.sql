drop table if exists player;

create table player
(
    id_player          long auto_increment primary key,
    ip_last_connect    varchar(45) not null,
    name_player        varchar(50) not null,
    max_points         integer default 0,
    numbers_of_wins    integer default 0,
    numbers_of_defeats integer default 0
);
